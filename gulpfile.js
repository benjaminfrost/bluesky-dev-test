var gulp = require('gulp');
var sass = require('gulp-sass');
concat = require('gulp-concat');

var PATHS = {
  sass: [
    'node_modules/foundation-sites/scss',
  ]
}

gulp.task('sass', function() {
  return gulp.src('app/scss/styles.scss')
    .pipe(sass({
      includePaths: PATHS.sass
    })).pipe(concat('styles.css'))
    .pipe(gulp.dest('app/css'))
});

gulp.task('watch', function() {
  gulp.watch('app/scss/**/*.scss', ['sass']);
});
